# Ballance Map Index

Welcome here\~

## Summary
Ballance Map Index collects all [Ballance](https://en.wikipedia.org/wiki/Ballance) homemade maps and makes some simple comments on them.

Each map's page follow some format. If you are confused by some terminology or you want to provide us with a new map page, please see the relevant format [here](/Format.md).

## Search
Enter the map you want to find in the upper left corner. Can't find the map you want? Ask us an issue [here](https://gitee.com/bearkidsteam/BallanceMapIndex/issues).

## Help us
The Ballance Map Index uses Gitbook-cli for creation. If you want to join us and help us improve this list, you can submit a Pull request [here](https://gitee.com/bearkidsteam/BallanceMapIndex/pulls).

Ballance Map Index project is run by the Bear Kids Team and Chinese Ballance Forum provides data. Click [here](/Participant.md) to find out who is involved in this project and if you want to join, we are always welcome.

All words are under MIT License.

Laste update at 4/6/2018.

