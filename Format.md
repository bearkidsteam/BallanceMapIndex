# Format
This page describe a map page's format and give some terminology's description here.

## Terminology
### Checkpoints' count
Checkpoints' count actually counts the number of sections instead of the number of flames.

### Anthorized map
You can set your map as an Authorized map to allow others to follow certain restrictions when using your map. This can be a map that requires paying, or it is not allowed to copy any elements from your level to defend your intellectual achievements.  
We recommend that you use Freedom permissions to regulate your map so that the Ballance community can develop better.  

### Suit for
At this stage, most maps only apply to Ballance. But in the future when some second-generation Ballance games are released, this field will describe which derivative versions of Ballance can play this map.  
Here is a list describing the list of games supported by "Suit for"

* Ballance
* Ballance Remix \(chris241097\)\(Obsolete, Stop developing\)

### MapPackage
Some Chinese Ballance players packaged a map pack that included most of the homemade maps to make it easy to download all maps at once without downloading them separately. If this field indicates yes, it means you can get it from the map package.

### Supported by
"Supported by" indicates the customs clearance record for the map can be accepted by which websites. Of course, the corresponding website may require you to create records according to their requirements when accepting records.  
Below is a list describing the currently supported websites.

* [Speedrun.com](https://www.speedrun.com/ballance) \(Global Ballance Records Website\)
* [ScoreManager](http://jxtoolbox.sinaapp.com) \(China's largest Ballance Records website\)

## Format
Each map page must include Quick View, Screenshots, Download, Videos and Comments.

* Quick View related tables can not be changed, you can view the existing map page to understand what need to be filled in the tables.
* Use internal links in Screenshots, save the picture in the Img folder and then reference
* China and foreign countries provide download addresses respectively. For China, please provide the addresses which is not banned by [GFW](https://en.wikipedia.org/wiki/Great_Firewall). For foreign countries, please use a multi-language-enabled, unlimited-speed network disk, such as Mega.
* Videos suggest to write a Youtube link and a Youku link.
* Please add more exciting comments in Comments, please use the source language to fill in the comments, without conversion to English.

