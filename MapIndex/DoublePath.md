# Double Path

## Quick view

|||
|:---|:---|
|Map name|Double Path|
|Original name|双重路径|
|Author|skb7757|
|Release date|2012|
|Checkpoints' count|6|
|Anthorized map|Freedom|
|Suit for|Ballance, Ballance Remix|
|Included in Map Package|Yes|
|Supported by|ScoreManager|

## Screenshots

![](../Img/DoublePath.jpg)

## Download
None

## Videos
* [Youku](http://m.youku.com/video/id_XNDM4MzU2MTUy.html?&source=https%3A%2F%2Fm.baidu.com%2Ffrom%3D2001a%2Fbd_page_type%3D1%2Fssid%3D0%2Fuid%3D)

## Comments

**qwezxc385** comments on Chinese Ballance Forum
>每小节分简单、困难两种走法。每个小节都有大捷径，sr非常快，hs考验技术
