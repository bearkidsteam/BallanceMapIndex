# City Street

## Quick view

|||
|:---|:---|
|Map name|City Street|
|Original name|城市街道|
|Author|邂逅宝贝2012|
|Release date|2013|
|Checkpoints' count|7|
|Anthorized map|Freedom|
|Suit for|Ballance|
|Included in Map Package|Yes|
|Supported by|None|

## Screenshots

![](../Img/CityStreet.jpg)

## Download
None

## Videos
* [Youku](http://v.youku.com/v_show/id_XNzA3NjA0NTg4.html)

## Comments

**qwezxc385** comments on Chinese Ballance Forum
>也是比较常规的一个图，虽然细节还是略为粗糙，但相对而言已经有很大进步了。对于新手很好玩。